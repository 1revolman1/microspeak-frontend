import io from "socket.io-client";
const linkSocket = "http://localhost:3001";
let nickname = null;

export const setNickname = function (nick) {
  nickname = nick;
};

export const getSocket = function () {
  console.log(nickname);
  return io(linkSocket, {
    path: "/api/socket",
    query: `nick=${nickname}`,
  });
};
