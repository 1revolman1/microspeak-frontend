import { handleActions } from "../../helpers/immer";
import * as action from "../../actions/Friend";
const initialValue = {
  data: {
    friendList: null,
    users: [],
    chats: null,
    socket: null,
    selectedChatIndex: null,
    selectedChatID: null,
  },
  chat: {
    messages: [],
    users: [],
  },
};
export const friend = handleActions(
  {
    //-----------INITIAL DATA-----------------
    [action.getInitialData]: (draft, { payload }) => {
      draft.data.chats = payload.chatArray;
      draft.data.friendList = payload.friendArray;
      draft.data.socket = payload.socket;
    },
    //-----------SELECT CHAT INDEX-----------
    [action.selectUserChat]: (draft, { payload }) => {
      const index = draft.data.chats.findIndex(
        (todo) => todo._id === payload.id
      );
      if (index !== -1 && draft.data.socket.connected) {
        draft.data.selectedChatIndex = index;
        draft.data.selectedChatID = payload.id;
        draft.data.socket.emit("connect-to-channel", payload.id);
      }
    },
    //-----------FIND DATA--------------------
    [action.findUser]: (draft, { payload }) => {
      draft.data.users = payload.users;
    },
    [action.getMyChatsFromServer]: (draft, { payload }) => {
      draft.data.chats = payload.chatArray;
    },
    //-----------SEND MESSAGE TO CHAT----------------
    [action.sendMessage]: (draft, { payload: { message } }) => {
      const id = draft.data.selectedChatID;
      draft.data.socket.emit("chat message to backend", { message, id });
    },
    //-----------GET MESSAGES FROM CHAT--------------
    [action.receiveMessages]: (draft, { payload }) => {
      draft.chat.messages = payload.msg;
      draft.chat.users = payload.users;
    },
  },
  initialValue
);
