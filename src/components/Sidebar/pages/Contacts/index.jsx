import React, { useRef } from "react";
import Searcher from "../../components/Searcher";
import CreateChat from "../../components/CreateChat";
import UserTab from "../../components/UserTab";
import { StyledUsers, StyledMenu } from "./styled";
import { useSelector, useDispatch } from "react-redux";
import { currentUser } from "../../../../reduxThunk/selector/User";
import {
  getUsers,
  getUserFriends,
} from "../../../../reduxThunk/selector/Friend";
import { friendFindUser } from "../../../../reduxThunk/actions/Friend";

export default function Chats() {
  const timer = useRef(null);
  //REDUX
  const dispatch = useDispatch();
  const users = useSelector(getUsers);
  const friends = useSelector(getUserFriends);
  const me = useSelector(currentUser);
  //REDUX

  const onInput = function (event) {
    let text = event.target.value;
    clearTimeout(timer.current);
    timer.current = setTimeout(() => {
      dispatch(friendFindUser(text));
    }, 3000);
  };
  return (
    <>
      <Searcher type="contacts" onInput={onInput} />
      <StyledUsers>
        {users && users.length > 0 && (
          <div className="users-list">
            <h1>It is the list of users that you searching:</h1>
            {users.map((user, index) => {
              if (user)
                return (
                  <UserTab
                    me={me.nickname}
                    type="USER"
                    key={`id${index}`}
                    self={false}
                    user={user}
                    online={user.isOnline}
                  />
                );
              else return null;
            })}
          </div>
        )}
        {friends && users && users.length === 0 && (
          <div className="friend-list">
            <h1>It is the list of your friends:</h1>
            {friends.map((user, index) => {
              if (user)
                return (
                  <UserTab
                    me={me.nickname}
                    type="USER"
                    key={`id${index}`}
                    self={false}
                    user={user}
                    online={user.isOnline}
                  />
                );
              else return null;
            })}
          </div>
        )}
      </StyledUsers>
      <StyledMenu>
        <CreateChat>
          <span> </span> Contacts
        </CreateChat>
        {/* <MenuNavigation /> */}
      </StyledMenu>
    </>
  );
}
